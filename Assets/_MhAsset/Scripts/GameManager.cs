using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class GameManager : MonoBehaviour
{


    [Separator("Spawn Birds")]
    public Bird birdPrefab;
    public Vector2 timeSpawn;
    public List<Transform> listPosSpawn;
    public Vector2 distanseSpawn;

    Vector2 pos;
    private void Start()
    {
        StartGame();
    }

    void Init()
    {

    }

    void StartGame()
    {
        SpawnBird();
    }

    public void SpawnBird()
    {
        pos = listPosSpawn[Random.Range(0, listPosSpawn.Count)].position;
        pos.y += Random.Range(distanseSpawn.x, distanseSpawn.y);
        Bird newBird = Instantiate(birdPrefab,pos, Quaternion.identity);
        newBird.Init(); 

        Invoke("SpawnBird", Random.Range(timeSpawn.x, timeSpawn.y));
    }
}
