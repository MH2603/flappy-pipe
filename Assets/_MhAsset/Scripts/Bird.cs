using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    public Rigidbody2D rb;

    public float speed = 1f;

    Vector2 vec = Vector2.zero;

    public void Init()
    {
        if (rb == null) rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0f;

        Move();
    }

    public void Move()
    {
        vec = Vector3.zero;
        vec.x = -speed;

        rb.velocity = vec;  
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if ( collision.gameObject.GetComponent<PipeController>() )
        {


            rb.velocity = Vector2.zero;
            rb.gravityScale = 5;
        }
    }
}

public enum birdStatus
{
    fly,
    die
}
