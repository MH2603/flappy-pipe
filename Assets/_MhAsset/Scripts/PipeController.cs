using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using System.Runtime.InteropServices;

public class PipeController : MonoBehaviour
{
    [Separator("Componnet")]
    public Rigidbody2D rb;

    [Separator("Setup")]
    public KeyCode keyJump = KeyCode.Space;
    public float gravity = 1f;
    public float vecAdd = 2f;
    public float maxSpeedFall = -3f;

    Vector2 vec;

    private void Start()
    {
        if( rb == null ) rb = GetComponent<Rigidbody2D>();  
    }

    private void Update()
    {
        if (Input.GetKeyDown(keyJump))
        {
            Jump();
        }

        Gravity();
    }

    public void Jump()
    {
        vec = Vector2.zero;
        vec.y = vecAdd;

        rb.velocity = vec;
    }

    void Gravity()
    {
        if (rb.velocity.y <= maxSpeedFall) return;
        vec.y -= gravity*Time.deltaTime;

        rb.velocity = vec;
    }
    
}
